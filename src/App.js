import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Image from './Imgae';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      page: 'home',
    };

    this.togglePage = this.togglePage.bind(this);
  }

  componentDidMount() {
    setInterval(this.togglePage, 5000)
  }

  togglePage() {
    this.setState({
      page: this.state.page === 'home' ? 'about' : 'home',
    });
  }

  render() {
    return (
      <div>
        <button onClick={this.togglePage}> swap </button>
       <ReactCSSTransitionGroup
          transitionName="background"
          transitionEnterTimeout={1000}
          transitionLeaveTimeout={1000}
        >
          <Image page={this.state.page} key={this.state.page} />
        </ReactCSSTransitionGroup>
      </div>
    );
  }
}

export default App;
